select
	orders.country,
	concat(invoices.address1, ' ', invoices.address2, ' ', invoices.city, ' ', invoices.country) as warehouse_address,
	invoices.phone as warehouse_phone,
	invoices.contact1 as warehouse_whatsapp,
	invoices.email as warehouse_email,
	invoices.taxnumber as warehouse_tax_number,
	invoices.vrn as warehouse_vrn,
	warehouses.warehousedescription as warehouse_description,
	
	orders.other13 as invoice_number,
	'' as barcode,
	format(orders.orderdate, 'yyyy-MM-dd HH:mm tt') as order_date,
	orders.orderid as order_id,
	orders.other11 as neolife_id,
	customers.field2 as customer_name,
	customers.vatregistration as customer_vat_registration,
	concat(customers.mainaddress1, ' ', customers.maincity, ' ', customers.mainstate, ' ', customers.maincountry) as address,
	customers.mobilephone as customer_phone,
	shipmethods.shipmethoddescription as ship_method,
	format(cast(orders.other20 as float), '#,0') as mtd_pv,
	concat(orders.other19, orders.other14) as call_no,
	orders.other17 as group_order_id,
	orders.taxtotal as total_tax,
	extended.otherextended8 as processor,
	concat('Please renew before ', format((select top 1 expiredate from customersubscriptions where customerid = orders.customerid and subscriptionid = 8 order by expiredate desc), 'yyy-MM-dd'), ' :: ', orders.notes) as notes,
	concat(orders.firstname, ' ', orders.lastname) as ship_name,
	concat(orders.address1, ' ', orders.address2, ' ', orders.address3, ' ', orders.city, ' ', orders.state, ' ', orders.zip, ' ', orders.country) as ship_address,
	orders.phone as ship_phone,
	customers.field6 as ship_sponsor_id,
	customers.field13 as ship_team_id,
	customers.field15 as ship_pt_id,
	format(orders.orderdate, 'yyyyMM') as sales_period,
	case when isnull(extended.otherextended10, 0) = 2 then 1 else 0 end as copy,

	format(orders.subtotal, '#,0.00') as sub_total,
	format(orders.shippingtotal, '#,0.00') shipping_total,
	format(orders.ordertax, '#,0.00') as tax,
	format(orders.total, '#,0.00') as total_due,
	format(orders.businessvolumetotal, '#,0.00') as total_pv,
	format(orders.commissionablevolumetotal, '#,0.00') as total_bv,
	format(orders.weighttotal, '#,0.00') as total_weight,
	format(orders.other7total, '#,0.00') as srp,

	case when items.field3 = 'S' then 'SINGLE' when items.field3 = 'C' then 'CASE' else '' end as uom,
	case when orderdetails.itemcode = 'Stamp Duty' then 1 else 0 end as stamp_duty,
	orderdetails.itemcode,
	orderdetails.itemdescription,
	orderdetails.quantity,
	orderdetails.businessvolume as pv,
	orderdetails.commissionablevolume as bv,
	format(orderdetails.other10each, '#,0.00') as unit_price,
	format(orderdetails.pricetotal, '#,0.00') as exclusive,
	format(orderdetails.tax, '#,0.00') as vat,
	format(orderdetails.other10, '#,0.00') as inclusive,

	(select count(orderid) from orders where customerid = customers.customerid and orders.orderstatusid in (7, 10) and orderid != orders.orderid) as unexecuted
	
from orderdetails
join orders on orders.orderid = orderdetails.orderid
join warehouses on warehouses.warehouseid = orders.warehouseid
join invoices.addresses invoices on invoices.warehouseid = warehouses.warehouseid
join customers on customers.customerid = orders.customerid
join shipmethods on shipmethods.shipmethodid = orders.shipmethodid
left join ordercalccontext.orderextendedfields extended on extended.orderid = orders.orderid
join items on items.itemcode = orderdetails.itemcode
where (orders.orderid = @p0 or orders.other17 = TRY_CAST(@p0 as nvarchar(50))) 
and orderdetails.itemcode != '10'