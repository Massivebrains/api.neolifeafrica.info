
merge into customservicescontext.smsmessages messages 
using (
	
	select
		messages.messageid,
		concat(substring(messages.message, 0, charindex('Password', messages.message)), ' Password : ****') as message
	from customservicescontext.smsmessages messages
	where messages.message like '%Password%'
	and messages.message not like '%****%'
	and messages.smsstatusid = 2
) update_messages
on messages.messageid = update_messages.messageid
when matched then update set messages.message = update_messages.message;

merge into customcustomercontext.customerpassword passwords 
using (
	
	select
		passwords.customerid,
		passwords.passwordtxt as password
	from customcustomercontext.customerpassword passwords
	join customservicescontext.smsmessages messages on messages.customerid = passwords.customerid
	where messages.smsstatusid = 2
	and messages.fieldname = 'eventid'
	and messages.message like '%Welcome%'
	and len(passwords.passwordtxt) > 0

) update_passwords
on passwords.customerid = update_passwords.customerid
when matched then update set passwords.passwordtxt = null;
