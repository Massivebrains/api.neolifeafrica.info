select top 500
	max(customerid) as customerid,
	max(neolifeid) as neolife_id,
	count(customerid) as count,
	-max(dayspurchased) as days
from customservicescontext.managesubscriptions where notes = 'DELETE-GRACE'
group by customerid
having count(customerid) > 1
and customerid not in (select customerid from customservicescontext.managesubscriptions where notes = 'FIX-DELETE-GRACE')