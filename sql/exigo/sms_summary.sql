select
	countries.countrydescription as country,
	(select count(messageid) from customservicescontext.smsmessages messages join customers on customers.customerid = messages.customerid where customers.maincountry = countries.countrycode and messages.smsstatusid = 0) as pending,	
	(select count(messageid) from customservicescontext.smsmessages messages join customers on customers.customerid = messages.customerid where cast(messages.modifieddate as date) = cast(getdate() as date) and customers.maincountry = countries.countrycode and messages.smsstatusid = 2) as sent
from customservicescontext.neolifecountries countries where countries.countrycode in ('NG', 'GH', 'CM', 'CI', 'KE', 'UG', 'TZ', 'KE', 'TG')
