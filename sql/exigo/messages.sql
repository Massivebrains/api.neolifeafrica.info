select distinct top 100
    customers.maincountry as country,
    customers.customerid,
    messages.fieldname as type,
    customers.field2 as name,
    messages.messageid as id,
    messages.tonumber as phone,
    messages.message as text,
    messages.messagedate as date,
    orders.orderid as order_id,
    customers.field1 as neolife_id,
    orders.firstname as name,
    customers.field6 as sponsor_id,
    passwords.loginname as login_name,
    passwords.passwordtxt as password,
    customersites.webalias as web_alias,
    sponsor.firstname as sponsor_first_name,
    sponsor.lastname as sponsor_last_name,
    orders.total as order_total,
    orders.other13 as invoice_number,
    orders.businessvolumetotal as pv,
    orders.commissionablevolumetotal as bv
from customservicescontext.smsmessages messages
join customers on customers.customerid = messages.customerid
left join orders on orders.orderid = messages.keyvalue
left join customers sponsor on  sponsor.field1 = customers.field6
left join customcustomercontext.customerpassword passwords on passwords.customerid = messages.customerid
left join customersites on customersites.customerid = customers.customerid
where customers.maincountry like concat('%', (case when  @p0 = 'all' then '' else @p0 end) , '%')
and messages.smsstatusid = 0
and isnull(messages.providermessageid, 0) = @p1