select
	customers.customerid,
	countries.countrydescription as country,
	warehouses.warehousedescription as warehouse,
	
	orders.other13 as invoice_number,
	format(orders.orderdate, 'yyyy-MM-dd HH:mm tt') as order_date,
	orders.orderid as order_id,

	orders.other11 as neolife_id,
	concat(customers.firstname, ' ', customers.lastname) as customer_name,
	customers.mobilephone as customer_phone,

	concat(partner.field1, ' ', partner.field3) as partner_name,

	customers.field6 as sponsor_id,
	customers.field13 as team,
	customers.field15 as pt,
	customers.field11 as rank,
	format(orders.orderdate, 'yyyyMM') as sales_period,

	orderdetails.itemcode,
	orderdetails.itemdescription,
	orderdetails.quantity,
	0 as status,
	isnull(vips.customerid, 0) as vip
	
from orderdetails
join orders on orders.orderid = orderdetails.orderid
join warehouses on warehouses.warehouseid = orders.warehouseid
join customers on customers.customerid = orders.customerid
join customservicescontext.neolifecountries countries on countries.countrycode = orders.country
left join customerextendeddetails partner on partner.customerid = customers.customerid and partner.customerextendedgroupid = 1
left join conventionvips vips on vips.customerid = customers.customerid and vips.conventioncountry = orders.country
where orders.orderid = try_convert(int, @p0)
and orderdetails.itemcode in ('272', '104', '411', '412', '446')
and orders.orderstatusid >= 7
and orders.orderdate > '2019-01-01'

union

select
	customers.customerid,
	countries.countrydescription as country,
	warehouses.warehousedescription as warehouse,
	
	orders.other13 as invoice_number,
	format(orders.orderdate, 'yyyy-MM-dd HH:mm tt') as order_date,
	orders.orderid as order_id,

	orders.other11 as neolife_id,
	concat(customers.firstname, ' ', customers.lastname) as customer_name,
	customers.mobilephone as customer_phone,

	concat(partner.field1, ' ', partner.field3) as partner_name,

	customers.field6 as sponsor_id,
	customers.field13 as team,
	customers.field15 as pt,
	customers.field11 as rank,
	format(orders.orderdate, 'yyyyMM') as sales_period,

	orderdetails.itemcode,
	orderdetails.itemdescription,
	orderdetails.quantity,
	0 as status,
	isnull(vips.customerid, 0) as vip
	
from orderdetails
join orders on orders.orderid = orderdetails.orderid
join warehouses on warehouses.warehouseid = orders.warehouseid
join customers on customers.customerid = orders.customerid
join customservicescontext.neolifecountries countries on countries.countrycode = orders.country
left join customerextendeddetails partner on partner.customerid = customers.customerid and partner.customerextendedgroupid = 1
left join conventionvips vips on vips.customerid = customers.customerid and vips.conventioncountry = orders.country
where cast(orders.other11 as nvarchar(10)) = cast(@p0 as nvarchar(10))
and orderdetails.itemcode in ('272', '104', '411', '412', '446')
and orders.orderstatusid >= 7
and orders.orderdate > '2019-01-01'