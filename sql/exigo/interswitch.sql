select
	orders.orderid as order_id,
	orders.total as amount,
	payments.memo as reference,
    112106 as account
from orders
join payments on payments.orderid = orders.orderid
join customservicescontext.neolifecountries nc on orders.country = nc.countrycode
join ordercalccontext.orderextendedfields extended on orders.orderid = extended.orderid
where orders.country = 'NG'
and orders.ordertypeid in (2, 11)
and format(orders.orderdate, 'yyyyMM') = @p0
and len(otherextended4) > 1
order by orders.orderdate asc;