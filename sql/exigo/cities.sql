select top 1
	max(countries.countrycode) as country,
    max(countryregions.regioncode) as region,
	concat(max(countryregions.regiondescription), ' ', max(countries.countrydescription)) as address,
	max(cities.regionlatitude) as latitude,
	max(cities.RegionLongitude) as longitude
from countryregions
join customservicescontext.neolifecountries countries on countries.countrycode = countryregions.countrycode
join exigowebcontext.countryregioncities cities on cities.regioncode = countryregions.regioncode and cities.countrycode = countryregions.countrycode
where countries.region = 'AFR' 
and cities.RegionLatitude = 0
group by cities.regioncode
order by max(countries.countrycode)