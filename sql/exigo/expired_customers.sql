select
	customers.customerid, 
	customers.field1 as neolife_id,
	datediff(day, subscriptions.expiredate, getdate()) as days,
	subscriptions.expiredate
from customers
join customservicescontext.neolifecountries countries on countries.countrycode = customers.maincountry
join customersubscriptions subscriptions on subscriptions.customerid = customers.customerid
where customers.customerid in (1078577)
order by customers.customerid