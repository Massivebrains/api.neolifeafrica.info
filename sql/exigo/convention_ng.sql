select top 1001
	orders.orderid as order_id,
	customers.field1 as neolife_id,
	orderdetails.itemcode as item_code,
	customers.field2 as customer_name,
	customers.field11 as rank,
	orderdetails.quantity as quantity,
	orders.returnorderid as return_id,
	customers.field13 as team_name,
	dbo.FormatPtString(customers.field15) as pt_name,
	july_volumes.volume2 as july_ppv,
	july_volumes.volume104 as july_personal_sponsoring,
	august_volumes.volume2 as august_ppv,
	august_volumes.volume104 as august_personal_sponsoring

from orderdetails
join orders on orders.orderid = orderdetails.orderid
join customers on customers.customerid = orders.customerid
left join periodvolumes july_volumes on july_volumes.customerid = orders.customerid and july_volumes.periodid = 53 and july_volumes.periodtypeid = 1
left join periodvolumes august_volumes on august_volumes.customerid = orders.customerid and august_volumes.periodid = 54 and august_volumes.periodtypeid = 1
where orderdetails.itemcode in ('294', '295', '296', '297', '414', '415', '416')
and orders.orderstatusid >= 7
and orders.country = 'NG'
and orders.orderid >= @p0
and orders.orderdate > '2020-01-01'
--and orderdetails.itemdescription like '%celebration%'
order by orders.orderid asc