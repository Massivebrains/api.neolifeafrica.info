select top 50
	paymentid as id,
	source,
	uniquereference as reference,
	countrycode,
	convert(varchar, depositdate, 22) as depositdate,
	amount,
	cast(description as nvarchar) as description
from payments
where uniquereference = @p0
or source = @p0
or cast(amount as nvarchar) like @p0
or cast(description as nvarchar) like concat('%', @p0, '%')
or countrycode = @p0
or customerid = @p0
order by depositdate asc