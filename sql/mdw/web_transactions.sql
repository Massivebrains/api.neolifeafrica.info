select
	cast(orderid as int) as order_id,
	amount,
	reference
from webpayments
where currencycode = @p0
and status = 'success'
and format(createdat, 'yyyyMMdd') between cast(@p1 as nvarchar) and cast(@p2 as nvarchar)