select
cast(orderid as int) as order_id, 
amount, 
reference 
from mpesa 
where status = 'completed' 
and format(createdat, 'yyyyMMdd') between cast(@p0 as nvarchar) and cast(@p1 as nvarchar)