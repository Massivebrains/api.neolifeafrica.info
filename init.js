const express = require('express');
var cors = require('cors');
const winston = require('winston');
const handlebars = require('express-handlebars');
const path = require('path');
const bodyparser = require('body-parser');

const home = require('./routes/home');
const payments = require('./routes/payments');
const posinvoice = require('./routes/posinvoice');
const sms = require('./routes/sms');
const subscriptions = require('./routes/subscriptions');
const convention = require('./routes/convention');
const mpesanotifications = require('./routes/mpesanotifications');
const airtelnotifications = require('./routes/airtelnotifications');
const sage = require('./routes/sage');
const subscription = require('./routes/subscription');

const logger = winston.createLogger({

    transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.Console({ format: winston.format.simple() })
    ]

});

module.exports = function (app) {

    app.use(express.json());
    app.use(bodyparser.urlencoded({extended : true}));
    app.use(cors());
    app.use('/assets', express.static(path.join(__dirname, './', '/views/assets')));
    app.use('/public', express.static(path.join(__dirname, '/public')));

    process.on('unhandledException', ex => logger.error(ex.message, ex));
    process.on('unhandledRejection', ex => logger.error(ex.message, ex));

    app.engine('hbs', handlebars({

        defaultLayout: 'app',
        layoutsDir: path.join(__dirname, '/views/layouts'),
        partialsDir: path.join(__dirname, '/views/layouts/partials'),
        extname: 'hbs'
    }));

    app.set('view engine', 'hbs');

    app.use(function(req, res, next) {

      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

      next();
  });

    app.use('/', home);
    app.use('/sms', sms);
    app.use('/payments', payments);
    app.use('/posinvoice', posinvoice);
    app.use('/subscriptions', subscriptions);
    app.use('/convention', convention);
    app.use('/kenya-notifications', mpesanotifications);
    app.use('/uganda-notifications', airtelnotifications);
    app.use('/sage', sage);
    app.use('/subscription', subscription);

    app.use(function (err, req, res, next) {

        logger.error(err, err);
        return res.status(500).json({ status: false, error: err });
    });
    
}