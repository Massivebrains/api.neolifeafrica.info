const fetch = require('node-fetch');

module.exports = {

  send: message => {

    return new Promise((resolve, reject) => {

      const endpoint = 'https://api.infobip.com/sms/1/text/single';
      const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Basic Z25sZGludDoxckBuc0UxMjM0`
      };

      const body = JSON.stringify({ from: 'NEOLIFE', to: message.phone, text: message.text });

      fetch(endpoint, { method: 'POST', headers: headers, body: body }).then(response => response.json()).then(response => {

        let status = response.messages[0].status;

        resolve({ status: status.groupName, description: status.description });

      }).catch(err => {

        reject(err);

      });

    });
  }

};
