const fetch = require("node-fetch");

module.exports = {

  send: message => {

    return new Promise((resolve, reject) => {

      let endpoint = `https://mtnmessenger.com/api/sms?username=nineolife&password=d33834&source=NEOLIFE`;
      endpoint += `&destination=${message.phone}&message=${message.text}`;

      fetch(endpoint).then(response => response.json()).then(response => {

        resolve({ status: true, description: response });
      
      }).catch(err => reject(err));
    
    });
  }
};
