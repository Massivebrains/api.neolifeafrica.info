const sql = require('mssql');
const fs = require('fs');
const config = require('config');

const exigo = config.get('database.exigo');

const db_config = {

  'user': exigo.user,
  'password': exigo.password,
  'server': exigo.server,
  'database': exigo.database,
  'connectionTimeout': 300000,
  'requestTimeout': 300000,
  pool:{ max : 4, min : 0, idleTimeoutMillis: 30000}

};


module.exports = {

  query: (query = '', p0 = null, p1 = null, p2 = null, p3 = null, p4 = null, p5 = null) => {

    return new Promise((resolve, reject) => {

      new sql.ConnectionPool(db_config).connect().then(pool => {

        let connection = pool.request();

        if(p0 != null) connection.input('p0', isNaN(p0) ? sql.NVarChar : sql.Int, p0);
        if(p1 != null) connection.input('p1', isNaN(p1) ? sql.NVarChar : sql.Int, p1);
        if(p2 != null) connection.input('p2', isNaN(p2) ? sql.NVarChar : sql.Int, p2);
        if(p3 != null) connection.input('p3', isNaN(p3) ? sql.NVarChar : sql.Int, p3);
        if(p4 != null) connection.input('p4', isNaN(p4) ? sql.NVarChar : sql.Int, p4);
        if(p5 != null) connection.input('p5', isNaN(p5) ? sql.NVarChar : sql.Int, p5);

        if(query.includes('.sql')){

          const query_string = fs.readFileSync(`./sql/${query}`, null);

          return connection.query(`${query_string}`);

        }else{

          return connection.query(query);

        }

      }).then(result => {

        sql.close();

        if(result.recordsets.length > 0)
          resolve(result.recordsets[0]);

        resolve(true);

      }).catch(err => {

        sql.close();
        reject(err);

      });

    });
  }

};
