module.exports = {

    formatMSISDN: phone => {

        phone = phone
        .replace('+', '')
        .replace('-', '')
        .replace('(', '')
        .replace(')', '')
        .replace(' ', '');

        if (phone.substring(0, 3) == '011')
            phone = phone.substring(3);

        if(phone.substring(0, 3) == '234')
            phone = '234' + phone.substring(phone.length - 10);

        return phone;

    },

    formatText: row => {

        return row.text
            .replace(' {%OrderID%}', row.order_id)
            .replace(' {%ORDERID%}', row.order_id)
            .replace('{%OrderBusinessVolume%} ', row.pv)
            .replace('{%ORDERBUSINESSVOLUME%} ', row.pv)
            .replace(' {%OrderTotal%}', row.order_total)
            .replace(' {%ORDERTOTAL%}', row.order_total)
            .replace('{%CustomerCompany%}', row.name)
            .replace('{%customercompany%}', row.name)
            .replace('{%CustomerLastName%}', '')
            .replace('{%CustomerFirstName%}', row.name)
            .replace(' {%NeolifeID%}', row.neolife_id)
            .replace(' {%CustomerField1%}', row.neolife_id)
            .replace('{%CustomerField1%}', row.neolife_id)
            .replace('{%ENROLLERFIRSTNAME%}', row.sponsor_first_name)
            .replace('{%ENROLLERLASTNAME%}', row.sponsor_last_name)
            .replace('{%WebAlias%}', row.web_alias)
            .replace('{%LoginName%}', row.login_name)
            .replace('{%Password%}', row.password)
            .replace('{%expiredate%}', '')
            .replace(/'/g, '')
            .replace('/', '')
            .replace('\\', '')
            .replace('#', '');
    }
}