var config = { apiKey: 'key-ca7bee539b8cd6d1e28cbdf231289040', domain: 'mdw.neolifeafrica.info' };
var mailgun = require('mailgun-js')(config);
const fs = require('fs');
const path = require('path');

module.exports = {

  send: message => {

    let from = "Notifications <notifications@mdw.neolifeafrica.info>";

    if(message.from)
      from = message.from;

    data = {
      from: from,
      to: message.to,
      subject: message.subject,
      html: message.body
    };

    if(data.to != 'oluwaseguno@ng.neolfie.com')
      data.bcc = 'oluwaseguno@ng.neolife.com';

    mailgun.messages().send(data, (err, body) => {

      if (err) console.log(err);

      return true;
    });

    return data;
  },

  sendWithAttachment: message => {

    var attachment = new mailgun.Attachment({

      data: fs.readFileSync(path.join(__dirname, '../public', message.file)),
      filename: message.file

    });
    

    data = {

      from: 'Notifications <notifications@mdw.neolifeafrica.info>',
      to: message.to,
      subject: message.subject,
      text: message.body,
      attachment: attachment

    };

     if(data.to != 'oluwaseguno@ng.neolfie.com')
      data.bcc = 'oluwaseguno@ng.neolife.com';


    mailgun.messages().send(data, (err, body) => {

      if (err) console.log(err);

      return true;

    });

    return data;
  }
};