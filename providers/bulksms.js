const fetch = require('node-fetch');

module.exports = {

  send: message => {

    return new Promise((resolve, reject) => {

      const endpoint = 'https://api.bulksms.com/v1/messages';

      const headers = {

        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Basic TmlnZXJpYXRlc3Q6I05pZ2VyaWF0ZXN0`

      };

      const body = JSON.stringify({
        from: 'NEOLIFE',
        to: message.phone,
        body: message.text
      });

      fetch(endpoint, { method: 'POST', headers: headers, body: body })
        .then(response => response.json())
        .then(response => {

          console.log(JSON.stringify(response));
          resolve(response);

        })
        .catch(err => {
          reject(err);
        });
    });
  },
  response: payload => {
    const status = payload.messages[0].status;

    //return {'status' : status.groupName, 'description' : status.description};
  }
};
