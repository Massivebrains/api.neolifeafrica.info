const sql = require('mssql');
const fs = require('fs');
const config = require('config');

const banks_api = config.get('database.banks_api');

const db_config = {

  'user': banks_api.user,
  'password': banks_api.password,
  'server': banks_api.server,
  'database': banks_api.database,
  'connectionTimeout': 300000,
  'requestTimeout': 300000,
  pool:{ max : 5, min : 0, iddle : 20000,  acquire : 20000 }

};

module.exports = {

  query: (query = '', p0 = null, p1 = null, p2 = null, p3 = null, p4 = null, p5 = null) => {

    return new Promise((resolve, reject) => {

      new sql.ConnectionPool(db_config).connect().then(pool => {

        let connection = pool.request();

        if(p0 != null) connection.input('p0', sql.NVarChar, p0);
        if(p1 != null) connection.input('p1', sql.NVarChar, p1);
        if(p2 != null) connection.input('p2', sql.NVarChar, p2);
        if(p3 != null) connection.input('p3', sql.NVarChar, p3);
        if(p4 != null) connection.input('p4', sql.NVarChar, p4);
        if(p5 != null) connection.input('p5', sql.NVarChar, p5);

        if(query.includes('.sql')){

          const query_string = fs.readFileSync(`./sql/${query}`, null);

          return connection.query(`${query_string}`);

        }else{

          return connection.query(query);

        }

      }).then(result => {

        sql.close();

        if(result.recordsets.length > 0)
          resolve(result.recordsets[0]);

        resolve(true);

      }).catch(err => {

        sql.close();
        reject(err);

      });

    });
  }

};
