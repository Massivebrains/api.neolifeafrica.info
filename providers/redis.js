const redis = require('redis');

//const client = redis.createClient(6379);

// client.on('error', error => {

//     console.log(`REDIS CLIENT Error : ${error}`);
// });

module.exports = {

    save : (key = null, value = null, type = 'object', expire = 0) => {

        return new Promise((resolve, reject) => {

            try{

                if(type == 'object')
                    value = JSON.stringify(value);

                if(value){
                    
                    client.del(key);

                    if(expire > 0){

                        client.set(key, value, 'EX', parseInt(expire));
        
                    }else{
        
                        client.set(key, value);
                    }

                    resolve(true);

                }else{

                    reject('Invalid Value. Value failed JSON.stringify()');
                }

            }catch(ex){

                reject(ex);
            }
            
        })
    },

    get : (key = null, type = 'object') => {

        return new Promise((resolve, reject) => {

            try{

                client.get(key, (error, value) => {
                    
                    if(value){
                        
                        if(type == 'object')
                            value = JSON.parse(value);

                        resolve(value);
                        
                    }else{
    
                        resolve(null);
                    }
                })

            }catch(ex){

                reject(ex);
            }
        })
    },

    getAll : () => {

        return new Promise((resolve, reject) => {

            try{

                client.keys('*', (error, keys) => {
                    
                    resolve(keys);
                })

            }catch(ex){

                reject(ex);
            }
        })
    }
}