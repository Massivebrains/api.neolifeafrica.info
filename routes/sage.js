const express = require('express');
const { Sage } = require('../controllers');

var router = express.Router();

router.get('/interswitch/:period', async (req, res, next) => {
  
  try{

    let response = await Sage.interswitch(req.params.period);

    res.json(response);

  }catch(ex){

    next(ex);
  }
  
});

router.get('/mpesa/:start_date/:end_date', async (req, res, next) => {
  
  try{

  	let {start_date, end_date} = req.params;

    let response = await Sage.mpesa(start_date, end_date);

    res.json(response);

  }catch(ex){

    next(ex);
  }
  
});

router.get('/web-payments/:currency/:start_date/:end_date', async (req, res, next) => {
  
  try{

  	let {currency, start_date, end_date} = req.params;

    let response = await Sage.webPayments(currency, start_date, end_date);

    res.json(response);

  }catch(ex){

    next(ex);
  }
  
});

module.exports = router;