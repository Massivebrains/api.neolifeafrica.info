const express = require('express');
var router = express.Router();
const config = require('config');

const { Payments } = require('../controllers');

router.get('/', (req, res) => {
    
  res.json({status : true, data : 'Use the /payments/search/{param} to search for payments'});
  
});

router.get('/search/:param', async (req, res, next) => {
  
  try{

    let response = await Payments.search(req.params.param);

    res.json(response);

  }catch(ex){

    next(ex);
  }
  
});

router.get('/sources', (req, res, next) => {

    return res.json(Payments.sources());
})

router.post('/login', async (req, res, next) => {
  
  try{

    let response = await Payments.login(req.body.username, req.body.password);

    return res.json(response);
    
  }catch(ex){
    
    next(ex);
  }
  
});

router.get('/customer/:neolife_id', async (req, res, next) => {
  
  try{
    
    let customer = await Payments.searchCustomer(req.params.neolife_id);

    res.json(customer);

  }catch(ex){
    
    next(ex);
  }
  
});

router.post('/save', async (req, res, next) => {
  
  try{
    
    let response = await Payments.savePayment(req.body);

    return res.json(response);
      
  }catch(ex){
    
    next(ex);
  }
  
});

router.get('/approve', async (req, res, next) => {

    try{
      
      let response = await Payments.approvePayment(req.query);

      return res.end(`<h1 style="font-family:arial; margin:50px auto; text-align:center; color:#222">${response}</h1>`);

    }catch(ex){
      
      return res.end(`<h1 style="font-family:arial; margin:50px auto; text-align:center; color:red; color:#222">${ex}</h1>`);
    }
  
});

router.post('/webpayments/update', async (req, res, next) => {
  
  try{
      
    let body = req.body;

    let response = await Payments.updateWebPayment(body.order_id, body.reference, body.password);

    res.json(response);

  }catch(ex){
    
    next(ex);
  }
  
});

router.post('/send-email', async (req, res, next) => {
  
  try{
      
    let body = req.body;

    if(!body.to || !body.subject || !body.body)
      return res.status(422).json({status: false, data: 'Invalid Payload' });

    let response = await Payments.sendEmail(body.to, body.subject, body.body, body.from);

    res.json({status: true, data: 'Email Sent Successfully'});

  }catch(ex){
    
    next(ex);
  }
  
});

module.exports = router;
