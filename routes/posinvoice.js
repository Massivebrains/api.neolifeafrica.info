const express = require('express');
var router = express.Router();
const config = require('config');
const { POSInvoice } = require('../controllers');

router.get('/:order_id?', async (req, res, next) => {

    try{
        
        let order_id = parseInt(req.params.order_id);

        if(req.query.json){

            let response = await POSInvoice.findInvoice(order_id);
            return res.json(response);
        }

        let response = await POSInvoice.getInvoice(order_id);
        
        return res.json(response);

    }catch(ex){

        res.json({status : false, data : ex });

    }
});

router.get('/update-copy/:order_ids?', async (req, res, next) => {

    try{
        
        let order_ids = parseInt(req.params.order_ids);

        if(!order_ids)
            return res.json({status : false, data: 'Set of Order IDs to mark as copy is required'});

        await POSInvoice.updateCopy(order_ids);

        return res.json({status : true, data : 'Order IDs marked as copy successfully. '});
        
    }catch(ex){

        res.json({status : false, data : ex });

    }
});



module.exports = router;