const express = require('express');
var router = express.Router();
const { Convention } = require('../controllers');
const redis = require('../providers/redis');

router.get('/:param', async (req, res, next) => {

    try {

        let response = await Convention.find(req.params.param);
        
        res.json(response);

    } catch (ex) {

        next(ex);
    }

});

router.post('/mark', async (req, res, next) => {

    try {

    	let order_id = req.body.order_id;
    	let item_code = req.body.item_code;

    	if(!order_id || !item_code)
    		return res.json({status : false, data: 'Order ID and Item Code is required to mark present. '});

        let response = await Convention.saveTicketToRedis(order_id, item_code);
        
        res.json({status : true, data : 'Ticket Marked as present succesfully '});

    } catch (ex) {

        next(ex);
    }

});

router.get('/search-ng/:param?', async (req, res, next) => {

    try {

        let response = await Convention.conventionNG(req.params.param);
        
        res.json(response);

    } catch (ex) {

        next(ex);
    }

});

router.get('/report/redis', async (req, res, next) => {

    try {

        let response = await redis.getAll();
        
        res.json(response);

    } catch (ex) {

        next(ex);
    }

});


module.exports = router;
