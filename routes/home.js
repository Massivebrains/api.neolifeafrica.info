const express = require('express');

var router = express.Router();

router.get('/', (req, res) => {

    res.json({status : true, data : { frontend_url : 'https://office.neolifeafrica.info' }});
})

module.exports = router;