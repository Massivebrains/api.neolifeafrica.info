const express = require('express');
const exigo = require('../providers/exigo');
const fetch = require('node-fetch');

var router = express.Router();

router.get('/', async (req, res, next) => {

    try {
        
        console.log(`Subscritpions Called @ ${new Date().toString()}`);
        
        let customers = await exigo.query('expired_customers.sql');

        if(customers.length < 1)
            return res.json({status : false, data : customers});

        let insert_query = 'insert into customservicescontext.managesubscriptions (customerid, subscriptionid, neolifeid, dayspurchased, notes) values ';

        customers.forEach(row => {

            if(row.days < 0){

                let query = `(${row.customerid}, 8, '${row.neolife_id}', ${row.days}, 'TERMINATE-STAFF-SUBSCRIPTION'),`;
                insert_query+= query;
            }
            
        });
        
        let insert = await exigo.query(insert_query.substr(0, insert_query.length - 1));
        
        console.log(`Insert completed @ ${new Date().toString()}`);
        const headers = { Accept: 'application/json', 'Content-Type': 'application/json' };
        let body = [];
        
        customers.forEach(row => body.push(row.customerid));
        
        // fetch(`https://mdw.neolifeafrica.info/mcommerce/api/customers/batch/update`, { method: 'POST', headers : headers, body: JSON.stringify(body) }).then(response => response.json()).then(response => {

        //     console.log(`Update customerstatusid completed @ ${new Date().toString()}`);
        //     return res.json(response);
            
        // }).catch(err => {

        //     console.log(`Error on Update customerstatusid completed ${err.toString()} @ ${new Date().toString()}`);
        //     return res.json({status : false, data : err});
        
        // });

        return res.json({status : true, data : customers.length});
        
            
    } catch (ex) {

        next(ex);

    }

});


router.get('/update-coordinates', async (req, res, next) => {

    try{

        let cities = await exigo.query('cities.sql');

        if(cities.length < 1) return res.json('NO more cities.');

        let city = cities[0];

        let responses = [];

        let sql_query = '';

        let endpoint = `https://maps.googleapis.com/maps/api/geocode/json?address=${city.address}&key=AIzaSyBxQR8cAuIWq8nwTVWjDzsDFVMqnaix8uo`;
        const headers = { Accept: 'application/json' };
                                    
        fetch(endpoint, {headers : headers }).then(response => response.json()).then(async response => {

            if(response && response.results && response.results.length > 0){

                let coordinates = response.results[0].geometry.location;

                responses.push(coordinates);

                sql_query += `update exigowebcontext.countryregioncities set regionlatitude = ${coordinates.lat}, regionlongitude = ${coordinates.lng} where countrycode = '${city.country}' and regioncode = '${city.region}' ;`;
                
                let update = await exigo.query(sql_query);

                console.log(sql_query);
                return res.json({status : true, data : {city : city, sql : sql_query, update : update }});
            
            }else{

                sql_query += `update exigowebcontext.countryregioncities set regionlatitude = 0, regionlongitude = 0 where countrycode = '${city.country}' and regioncode = '${city.region}' ;`;
                
                await exigo.query(sql_query);

                console.log(sql_query);

                console.log(`${endpoint}`);
                return res.json({status : false, data : endpoint});
            }

            
        }).catch(err => {
            
            console.log(err);
            
        });

        

    }catch(ex){


    }
})

module.exports = router;
