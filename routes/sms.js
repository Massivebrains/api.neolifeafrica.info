const express = require('express');
var router = express.Router();
const moment = require('moment');
const { SMS } = require('../controllers');

router.get('/', async(req, res, next) => {

  res.redirect('https:/mdw-services.netlify.com');

});

router.get('/summary', async (req, res, next) => {

  try{

    let messages = await SMS.summary();    

    return res.json({status : true, data : messages });

  }catch(ex){

    return res.json({status : false, data : ex.message});
  }

});


router.get('/merge-fields', async (req, res, next) => {

  try {

    let response = await SMS.mergeFields();

    return res.json(response);
    
  } catch (ex) {

    next(ex);

  }

});

router.get('/fetch/:country?/:update?', async (req, res, next) => {

  try {

    let response = await SMS.fetch(req.params.country, req.params.update);
    
    return res.json(response);

  } catch (ex) {

    next(ex);

  }

});

router.post('/report', async(req, res, next) => {

  try{

    let response = await SMS.report(req.body);

    return res.json(response);

  }catch(ex){

    next(ex);
  }

});

router.post('/bulk-sms', async (req, res, next) => {

  try{

    let neolife_ids = req.body.neolife_ids;
    let message = req.body.message_body;

    if(!neolife_ids)
      res.status(422).json({status : false, data : 'Comma separated Neolife IDs is required.'});

    if(!message)
      res.status(422).json({status : false, data: 'Message Body is required.' });

    let customers = await SMS.getCustomersFromNeolifeIds(neolife_ids);


    if(customers.length < 1)
      return res.status(400).json({status : false, data : 'No Customers found in list of Neolife IDs. Confirm your comma separated Neolife IDs is valid.'});

    let response = await SMS.insertMessages(customers, message);

    return res.json({status : true, data: `${customers.length} Messages inserted successfully. They would be dispatched accordingly.` });

  }catch(ex){

    next(ex);
  }

});

module.exports = router;
