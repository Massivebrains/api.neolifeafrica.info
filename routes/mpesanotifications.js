const express = require('express');
const { MpesaNotifications } = require('../controllers');

var router = express.Router();

router.post('/', async (req, res, next) => {

	try{

		let response = await MpesaNotifications.process(req.body);

		res.json(response);

	}catch(ex){

		next(ex);
	}
})

module.exports = router;