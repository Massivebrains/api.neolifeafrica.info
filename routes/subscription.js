const express = require('express');
var router = express.Router();
const { Subscription } = require('../controllers');

router.post('/create', async (req, res, next) => {

  try{

    let response = await Subscription.create(req.body);    

    return res.json(response);

  }catch(ex){

    return res.json({status : false, data : ex.message || ex.data });
  }

});

module.exports = router;