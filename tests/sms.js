const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

chai.use(chaiHttp);
chai.should();
chai.expect();

describe('SMS Routes Tests', () => {

	it('Get /summary Should return valid json', (done) => {

		chai
		.request(app)
		.get('/sms/summary')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

	it('Get /merge-fields Should return valid json', (done) => {

		chai
		.request(app)
		.get('/sms/merge-fields')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

	it('Get /fetch/NG Should return valid json', (done) => {

		chai
		.request(app)
		.get('/sms/fetch/NG')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

	it('Get /report Should return valid json', (done) => {

		chai
		.request(app)
		.post('/sms/report')
		.set('content-type', 'application/json')
		.send({})
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

});