const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

chai.use(chaiHttp);
chai.should();
chai.expect();

describe('Payments Routes Tests', () => {

	it('GET / Should return valid json', (done) => {

		chai
		.request(app)
		.get('/payments')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

	it('GET /search/{param} Should search payments correctly', (done) => {

		chai
		.request(app)
		.get('/payments/search/TESTPOS')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			done();
		});
	});

	it('GET /sources Should get payment sources', (done) => {

		chai
		.request(app)
		.get('/payments/sources')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			done();
		});
	});

	it('POST /login Should not login user', (done) => {

		chai
		.request(app)
		.post('/payments/login')
		.set('content-type', 'application/json')
		.send({username: 'test', password: 'test'})
		.end((err, res) => {

			res.should.have.status(500);
			res.body.should.be.a('object');
			res.body.should.have.property('status', false);
			done();
		});
	});

	it('POST /login Should login user successfully', (done) => {

		chai
		.request(app)
		.post('/payments/login')
		.set('content-type', 'application/json')
		.send({username: 'africa_it_olaiyas', password: 'V1ct0ry'})
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			done();
		});
	});

	it('POST / Should login user successfully', (done) => {

		chai
		.request(app)
		.post('/payments/login')
		.set('content-type', 'application/json')
		.send({username: 'africa_it_olaiyas', password: 'V1ct0ry'})
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			done();
		});
	});

	it('GET /customer/{param} Should get customer info', (done) => {

		chai
		.request(app)
		.get('/payments/customer/063-0863717')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			done();
		});
	});

	// it('POST /save Should get customer info', (done) => {

	// 	chai
	// 	.request(app)
	// 	.get('/payments/save')
	// 	.end((err, res) => {

	// 		res.should.have.status(200);
	// 		res.body.should.be.a('object');
	// 		res.body.should.have.property('status', true);
	// 		res.body.should.have.property('data');
	// 		done();
	// 	});
	// });

	// it('POST /approve Should get customer info', (done) => {

	// 	chai
	// 	.request(app)
	// 	.get('/payments/approve')
	// 	.end((err, res) => {

	// 		res.should.have.status(200);
	// 		res.body.should.be.a('object');
	// 		res.body.should.have.property('status', true);
	// 		res.body.should.have.property('data');
	// 		done();
	// 	});
	// });

});