const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

chai.use(chaiHttp);
chai.should();
chai.expect();

describe('Mpesa Notifications Routes Tests', () => {

	it('POST / Should return valid json', (done) => {

		chai
		.request(app)
		.post('/mpesa-notifications')
		.set('content-type', 'application/json')
		.send({data: {refrence : 'reference-number'}})
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});
});