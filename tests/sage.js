const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

chai.use(chaiHttp);
chai.should();
chai.expect();

describe('Sage Routes Tests', () => {

	it('Get /interswitch Should return valid json', (done) => {

		chai
		.request(app)
		.get('/sage/interswitch/201907')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

	it('Get /mpesa Should return valid json', (done) => {

		chai
		.request(app)
		.get('/sage/mpesa/20190601/20190610')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

	it('Get /web-payments Should return valid json', (done) => {

		chai
		.request(app)
		.get('/sage/web-payments/ghs/20190601/20190610')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			done();
		});
	});

});