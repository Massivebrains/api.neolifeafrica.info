const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

chai.use(chaiHttp);
chai.should();
chai.expect();

describe('POS Invoice Routes Tests', () => {

	describe('Get /', () => {


		it('should fail on validation', (done) => {

			chai
			.request(app)
			.get('/posinvoice/0')
			.end((err, res) => {

				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('status', false);
				res.body.should.have.property('data');
				done();
			});
		});

		it('should return invoice data', (done) => {

			chai
			.request(app)
			.get('/posinvoice/1949423')
			.end((err, res) => {

				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('status', true);
				res.body.should.have.property('data');
				done();
			});
		});

	});

	it('Get / should update copy', (done) => {

		chai
		.request(app)
		.get('/posinvoice/update-copy/1949423')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			done();
		});
	});

});