const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

chai.use(chaiHttp);
chai.should();
chai.expect();

describe('Convention Routes Tests', () => {

	describe('Get /{param}', () => {

		it('Should return no tickets found', (done) => {

			chai
			.request(app)
			.get('/convention/0')
			.end((err, res) => {

				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('status', false);
				res.body.should.have.property('data');
				done();
			});
		});

		it('Should return valid tickets', (done) => {

			chai
			.request(app)
			.get('/convention/1949423')
			.end((err, res) => {

				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('status', true);
				res.body.should.have.property('data');
				res.body.data[0].customerid.should.equal(139916);
				done();
			});
		});

	})
	

	it('POST /mark Should mark ticket successfully', (done) => {

		chai
		.request(app)
		.post('/convention/mark')
		.set('content-type', 'application/json')
		.send({order_id: 1949423, item_code: 272})
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			done();
		});
	});

	it('Get /search-ng/1948059 Should search for Nigerian tickets', (done) => {

		chai
		.request(app)
		.get('/convention/search-ng/1948059')
		.end((err, res) => {

			res.should.have.status(200);
			res.body.should.be.a('object');
			res.body.should.have.property('status', true);
			res.body.should.have.property('data');
			res.body.data[0].orderid.should.equal(1948059);		
			done();
		});
	});
});