const express = require('express');
const app = express();

require('./init')(app);

const port = process.env.PORT || 8080;

app.listen(port, () => console.log(`Magic happens on port ${port}`));

module.exports = app;
