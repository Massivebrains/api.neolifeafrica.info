const exigo = require('../providers/exigo');
const _ = require('lodash');
const bwipjs = require('bwip-js');

const POSInvoice = {

    getInvoice : async (order_id = 0) => {

        return new Promise(async (resolve, reject) => {

            try{

                if(!order_id || parseInt(order_id) < 1) 
                    return resolve({status : false, data : `Order ID not found. Go back to Intranet and try again.`});


                details  = await exigo.query('exigo/invoice.sql', order_id);

                let refresh = false;

                if(details.length > 0){

                    details.forEach(row => {

                        if(row.invoice_number == '') refresh = true;  
                    });

                    if(refresh) return resolve({status : false, data : `One ore more orders are yet to have Invoice Numbers` });
                }

                if(details.length < 1) 
                    return resolve({status : false, data : `Order ${order_id} Not Found. Kindly return back to Intranet`});

                let orders = _.uniq(_.map(details, 'order_id'));

                let invoices = [];
                let promises = [];

                orders.forEach(async order_id => {

                    let order_details = _.filter(details, { order_id : order_id });
                    let total_quantity = _.sumBy(order_details, 'quantity');
                    let order =  _.find(details, {order_id : order_id});

                    promises.push(POSInvoice.getBarCode(order));

                    invoices.push({ order : order, order_details : order_details, total_quantity : total_quantity });

                });

                Promise.all(promises).then(function(values){

                    let language = POSInvoice.lang(invoices[0].order.country);

                    resolve({status: true, data : invoices, order_ids : orders.join(','), language : language });

                }).catch(error => {

                    reject({status : false, data: error, order_ids : ''  });
                });

            }catch(ex){

                reject({status : false, data: ex, order_ids : '' });
            }
        })
    },

    getBarCode : async(order) => {

        let barcode_config = {bcid: 'code128', text: '0', includetext: true, textxalign: 'center' };

        return new Promise(async (resolve, reject) => {

            barcode_config.text = `${order.order_id}`;

            bwipjs.toBuffer(barcode_config, (err, png) => {

                if(err){

                    reject(err);

                }else{

                    order.barcode = png.toString('base64');
                    resolve(true);   
                }

            });
        });

    },

    updateCopy : async (order_ids = '') => {

        return new Promise(async (resolve, reject) => {

            let query = `update ordercalccontext.orderextendedfields set otherextended10 = 2 where orderid in(${order_ids})`;
            let update = await exigo.query(query);

            if(update){

                resolve(true);

            }else{

                reject(false);
            }

        });
    },

    findInvoice : async (order_id = 0) => {

        return new Promise(async (resolve, reject) => {

            try{

                let comment = 'Invoice is now avaliable for rendering...';

                details  = await exigo.query('exigo/invoice.sql', order_id);

                if(details.length < 1){

                    return resolve({status : false, data : 'This order may have been cancelled or does not exist.' });
                }

                let status = true;
                let i = 0;

                details.forEach(row => {

                    if(row.invoice_number == ''){

                        status = false; 
                        comment = 'One or more invoices in this Order does not have an Invoice Number Yet.';
                    }

                    i++;

                    if(i == details.length)
                        resolve({status : status, data : comment });
                });

            }catch(ex){

                reject({status : false, data : ex.message });
            }
        })
    },

    lang: (country = 'NG') => {

        const english = {

            'tax_invoice': 'TAX INVOICE',
            'team': 'TEAM',
            'address': 'ADDRESS',
            'inv': 'INV',
            'date': 'DATE',
            'ship': 'SHIP',
            'processor': 'PROCESSOR',
            'sales_period': 'SALES PERIOD',
            'vat': 'VAT',
            'price': 'PRICE',
            'subtotal': 'SUBTOTAL',
            'shipping': 'SHIPPING',
            'total_due': 'TOTAL DUE',
            'total_payment': 'TOTAL PAYMENT',
            'total_weight': 'TOTAL WEIGHT',
            'please_renew': 'Please renew before',
            'pickup_notes': 'KINDLY PICKUP YOUR PRODUCT(S) ONCE YOU HAVE RECEIVED THIS INVOICE. THANK YOU FOR CHOOSING NEOLIFE',
            'received_by': 'RECEIVED BY',
            'picklist': 'PICKLIST',
            'picked_by' : 'PICKED BY',
            'dispatched_by': 'DISPATCHED BY',
            'case': 'CASE',
            'single': 'SINGLE'
        };

        const french = {

            'tax_invoice': 'FACT.FISCALE',
            'team': 'EQUIPE',
            'address': 'ADRESSE',
            'inv': 'FACT',
            'date': 'DATE',
            'ship': 'EXP.',
            'processor': 'TRAITE PAR',
            'sales_period': 'PERIODE-VENTE',
            'vat': 'TVA',
            'price': 'PRIX',
            'subtotal': 'SOUS TOTAL',
            'shipping': 'EXP.',
            'total_due': 'TOTAL DU',
            'total_payment': 'PAIEMT TOTAL',
            'total_weight': 'POIDS TOTAL',
            'please_renew': 'Svp renouveller avt',
            'pickup_notes': 'PRIERE DE RECUPERER VOS PRODUITS DES RECEPTION DE VOTRE FACTURE. MERCI D AVOIR CHOISI NEOLIFE',
            'received_by': 'RECU PAR ',
            'picklist': 'PICKLIST',
            'picked_by' : 'CHOISI PAR',
            'dispatched_by': 'LIVRE PAR',
            'case': 'CAISSE',
            'single': 'UNITE'
        };

        if(country == 'TG')
            return french;

        return english;
    }
}

module.exports = POSInvoice;
