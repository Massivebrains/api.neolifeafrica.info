const mdw = require('../providers/mdw');
const exigo = require('../providers/exigo');
const fetch = require('node-fetch');
const FormData = require('form-data');
const _ = require('lodash');
const mailgun = require('../providers/mailgun');
const hbs = require('express-handlebars').create();
const moment = require('moment');
const infobip = require('../providers/infobip');
const config = require('config');

module.exports = {

    search : async (query = 'NG') => {

        return new Promise(async (resolve, reject) => {

            try{

                let payments = await mdw.query('mdw/payments.sql', query);
    
                resolve({status : true, data : payments });
    
            }catch(ex){
    
                reject(ex);
    
            }
        })
    },

    sources : () => {

        let data = {

            sources : config.get('payments.sources')
        };

        return{status : true, data : data};
    },

    login : async (username = '', password = '', warehouse = 'NG-Lagos Gbagada') => {

        return new Promise(async (resolve, reject) => {

            try{
                
                const endpoint = config.get('payments.login_url')
                
                let form = new FormData();
                form.append('username', username);
                form.append('password', password);
                form.append('warehouse', 'NG-Lagos Gbagada');
                
                fetch(endpoint, { method: 'POST', body: form }).then(response => response.json()).then(response => {
                    
                    if(response == true)
                        return resolve({status : true, data : 'Login Successful', username });

                    return reject({status : false, data : 'Invalid Username or Password' });
                    
                    
                }).catch(err => {

                    reject(err);

                });
                
            }catch(ex){
                
                reject(ex);
            }
        })
    },

    searchCustomer : async (neolife_id = '') => {

        return new Promise(async (resolve, reject) => {

            try{

                let customers = await  exigo.query('exigo/customer.sql', neolife_id);
            
                if(customers == undefined || customers.length < 1){

                    resolve({status : false, data : 'Customer Not Found' });

                }else{

                    resolve({ status: true, data: customers[0] })
                }

            }catch(ex){

                reject(ex);
            }
            
        })
    },

    savePayment : async (data) => {

        return new Promise(async (resolve, reject) => {

            try{
                
                let error = validatePayment(data);
            
                if(error.length > 0) return reject({status : false, data : error });
            
                data.reference = moment().unix() * Math.floor(Math.random() * 1000) + 1;
                data.url = `${config.get('app.base_url')}/payments/approve?`;
                data.url += `i=${data.neolife_id}&r=${data.reference}&a=${data.amount}&s=${data.source}&p=${data.phone}&u=${data.username}`;
                let emails = config.get('payments.approval_emails');
                                
                hbs.render('./views/payments/email.hbs', {data : data}).then(async html => {
            
                    mailgun.send({ to: emails, subject: `New Deposit Awaiting approval <created by ${data.username}>`, body:html });
            
                    resolve({status : true, data : 'Deposit assigned succesfully. Relax, let the assigned user complete the job!'});
            
                }).catch(async ex => {
            
                    reject(ex);
                });

            }catch(ex){
                
                reject(ex);
            }
        })
    },

    approvePayment : async (data) => {
        
        return new Promise(async (resolve, reject) => {

            try{

                let { i : neolife_id, r : reference, a : amount, s : source, p : phone, u : user} = data;

                if(neolife_id.length < 1 || parseFloat(amount) < 1 || reference.length < 1 || source.length < 1 || phone.length < 1){

                    return reject(`This Payment cannot be approved. It does not pass validation check!`);
                }

                const endpoint = config.get('payments.payment_url');
                const headers = { Accept: 'application/json', 'Content-Type': 'application/json', api_token : `tOHZEuhmnW7OpszH` };
                let search = await mdw.query('mdw/payments.sql', reference);

                if(search.length > 0){

                    return reject(`This Payment has already been approved or payment reference is duplicate!`);
                }

                const body = {

                    reference : reference,
                    customerid : neolife_id,
                    date : moment().toISOString(),
                    amount : amount,
                    source : source,
                    description : `CREATED VIA MDW`,
                };

                fetch(endpoint, { method: 'POST', headers : headers, body: JSON.stringify(body) }).then(response => response.json()).then(async response => {

                    if(response.status == false){

                        return reject(response.response);

                    }else{

                        let sms = `A Deposit of ${amount} has been created for ${neolife_id}. Kindly use ${reference} to claim this at any of our offices. Thank You.`;
                        await infobip.send({phone : phone, text : sms});

                        let userdata = await exigo.query(`select * from users where loginname = '${user}'`);
                        
                        if(!userdata || userdata.length < 1)
                            return resolve(response.response);

                        let email =  userdata[0].Email;

                        if(email.length > 0){

                            hbs.render('./views/payments/response-email.hbs', {data : body}).then(async html => {

                                mailgun.send({ to: [email], subject: `Your Deposit for <${body.customerid}> has been approved!`, body:html });

                            }).catch(async ex => {
                            
                                return reject(ex);
                            });
                        }

                        resolve(response.response);

                    }

                }).catch(err => {

                    reject(err.message);

                });

            }catch(ex){

                reject(ex.message);
            }
        })
    },

    updateWebPayment : async (order_id = 0, reference = '', password = '') => {

        return new Promise(async (resolve, reject) => {

            try{

                if(order_id < 1 || reference.length < 1 || password != 'AfMc1TgDjfZpRI'){

                    return resolve({status : false, data : `Invalid Payload. OrderID, Reference or Password is Invalid.` });
                }

                let payment = await  mdw.query(`select orderid, status from webpayments where orderid = ${order_id}`);
            
                if(payment == undefined || payment.length < 1){

                    return resolve({status : false, data : `Payment Transaction with Order ID ${order_id} not Found.` });

                }

                payment = payment[0];

                if(payment.status == 'success'){

                    return resolve({status : false, data : `Payment Transaction with Order ID ${order_id} is already successful.` });
                }

                let response = await  mdw.query(`update webpayments set status = 'success', reference = '${reference}' where orderid = ${order_id}`);

               return resolve({status : true, data : `Payment Transaction with Order ID ${order_id} Updated successfully.` });

            }catch(ex){

                reject(ex);
            }
            
        })
    },

    sendEmail : async (to = [], subject = '', body = '', from = null) => {

        return new Promise((resolve, reject) => {

            mailgun.send({ to, subject, body, from });

            resolve(true);
        })
    }
}

validatePayment = (data) => {

    let error = '';

    if(parseFloat(data.amount) < 1) 
        error = 'Amount is Required and must me more than zero.';

    if(data.source.length < 1) 
        error = 'Source is Required';

    if(data.phone.length < 13) 
        error = 'Phone Number is invalid (Include the country code e.g 234 if you do not)';

    if(data.username.length < 1) 
        error = 'You have to be logged in to crate a payment.';
            
    return error;
}
