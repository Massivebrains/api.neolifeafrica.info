const exigo = require('../providers/exigo');
const mdw = require('../providers/mdw');

const Sage = {

    interswitch: async (period = '') => {

        return new Promise(async (resolve, reject) => {

            try{
                
                let response  = await exigo.query('exigo/interswitch.sql', period);

                resolve({status: true, data: response});

            }catch(ex){

                reject({status : false, data: ex });
            }
        })
    },

    mpesa: async (start_date = '', end_date = '') => {

        return new Promise(async (resolve, reject) => {

            try{
                
                let response  = await mdw.query('mdw/mpesa_web_transactions.sql', start_date, end_date);

                resolve({status: true, data: response});

            }catch(ex){

                reject({status : false, data: ex });
            }
        })
    },

    webPayments: async (currency = '', start_date = '', end_date = '') => {

        return new Promise(async (resolve, reject) => {

            try{
                
                let response  = await mdw.query('mdw/web_transactions.sql', currency, start_date, end_date);

                resolve({status: true, data: response});

            }catch(ex){

                reject({status : false, data: ex });
            }
        })
    }
}

module.exports = Sage;
