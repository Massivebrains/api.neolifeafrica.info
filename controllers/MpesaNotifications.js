const mailgun = require('../providers/mailgun');
const fetch = require('node-fetch');
const moment = require('moment');
const config = require('config');

const MpesaNotifications = {

    process: async (data = null) => {

        return new Promise(async (resolve, reject) => {

            try{

                let reference = data.TransID;
                let amount = data.TransAmount;
                let phone = data.MSISDN;
                let neolife_id = data.BillRefNumber;

                if(!reference || !amount){

                    return reject({status: false, data: 'Reference and Amount is required.' });
                }

                const endpoint = config.get('payments.payment_url');
                const headers = { Accept: 'application/json', 'Content-Type': 'application/json', api_token : `tZDZf9oSVtb56d3P` };

                const body = {

                    reference : reference,
                    customerid : neolife_id,
                    date : moment().toISOString(),
                    amount : amount,
                    source : 'Mpesa',
                    description : `DESC: ${neolife_id} ${phone}`
                };

                fetch(endpoint, { method: 'POST', headers : headers, body: JSON.stringify(body) })
                .then(response => response.json())
                .then(async response => {

                    return resolve({status: true, data: response.response});

                })
                .catch(err => {

                    reject({status: false, data: err.message });

                });

            }catch(ex){

                reject({status: false, data: ex });
            }
        })
    }

}

module.exports = MpesaNotifications;