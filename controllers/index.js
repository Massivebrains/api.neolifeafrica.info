module.exports = {

    Payments: require('./Payments'),
    POSInvoice: require('./POSInvoice'),
    SMS: require('./SMS'),
    Convention: require('./Convention'),
    MpesaNotifications: require('./MpesaNotifications'),
    AirtelNotifications: require('./AirtelNotifications'),
    Sage: require('./Sage'),
    Subscription: require('./Subscription')
}