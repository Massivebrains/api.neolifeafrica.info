const exigo = require('../providers/exigo');
const redis = require('../providers/redis');

const Convention = {

    find : async (query = null) => {

        return new Promise(async (resolve, reject) => {

            try{

                if(!query) 
                    return resolve({status : false, data : `Search parameter is required.`});

                
                let tickets  = await exigo.query('exigo/ticket.sql', query);

                if(tickets.length < 1) 
                    return resolve({status : false, data : `No ticket found for ${query}`});

                let promises = [];

                tickets.forEach(async row => promises.push(Convention.updateTicketStatus(row)));

                Promise.all(promises).then(function(values){

                    resolve({status : true, data : tickets });

                }).catch(ex => {

                    console.log(ex);

                    reject({status : false, data: ex });
                });

            }catch(ex){

                console.log(ex);
                reject({status : false, data: ex });
            }
        })
    },

    updateTicketStatus : async (ticket) => {

        return new Promise(async (resolve, reject) => {

            try{

                let check = await redis.get(`ticket_${ticket.order_id}_${ticket.itemcode}`, 'string');

                resolve(ticket.status = check ? 1 : 0);

            }catch(ex){

                reject(false);
            }
        });
    },

    saveTicketToRedis : async (order_id = null, item_code = null) => {

        return new Promise(async (resolve, reject) => {

            if(order_id == null || item_code == null)
                return reject(false);

            let ttl = 60 * 60 * 24 * 10;
            await redis.save(`ticket_${order_id}_${item_code}`, 'present', 'string', ttl);

            resolve(true);

        });
    },

    conventionNG : async (query = null) => {

        return new Promise(async (resolve, reject) => {

            try{
                
                query = parseInt(query);
                let tickets  = await exigo.query('exigo/convention_ng.sql', query > 0 ? query : 0);

                if(tickets.length < 1)
                    return resolve({status : false, data : `No ticket found for ${query}`});

                resolve({status : true, data : tickets });

            }catch(ex){

                reject({status : false, data: ex });
            }
        })
    },
}

module.exports = Convention;
