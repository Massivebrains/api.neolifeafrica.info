const infobip = require('../providers/infobip');
const mtn = require('../providers/mtn');
const exigo = require('../providers/exigo');
const { formatMSISDN, formatText } = require('../providers/utils');
const jsonexport = require('jsonexport');
const fs = require('fs');
const mailgun = require('../providers/mailgun');
const moment = require('moment');
const _ = require('lodash');
const config = require('config');

module.exports = {

    summary : async () => {

        return new Promise(async (resolve, reject) => {

            try{

                let messages = await exigo.query('exigo/sms_summary.sql');    

                resolve(messages);

            }catch(ex){

                reject(ex.message);
            }
        })
    },

    mergeFields : async () => {

        return new Promise(async (resolve, reject) => {

            try {

                let query = '';
                let messages = await exigo.query('exigo/messages.sql', 'all', 0);
                let zeroPVOrders = [];

                messages.forEach(row =>  {

                    if(row.type == 'EventID' && row.login_name == null){

                        //console.log('No Login Name and Password Yet');

                    }else{

                        query += `update customservicescontext.smsmessages set message = '${formatText(row)}', tonumber = '${formatMSISDN(row.phone, row.country)}', providermessageid = 1 where messageid = '${row.id}'; `;

                        if(row.order_id > 0 && row.pv == 0) 
                            zeroPVOrders.push(row); 
                    }
                    
                });
                
                await exigo.query(query);

                if(zeroPVOrders.length > 0){

                    let ids = '';
                    zeroPVOrders.forEach(row => (ids += `'${row.id}',`));
                    let updateQuery = `update customservicescontext.smsmessages set smsstatusid = 4  where messageid in (${ids.substr(0, ids.length - 1)})`;
                    await exigo.query(updateQuery);

                }
                
                 //Also Update Passwords to **** for sent messages and also set passwords to null in the passwords table
                 //await exigo.query('exigo/passwords.sql');

                 resolve({ status: true, data: messages.length });

             } catch (ex) {

                reject(ex);
            }
        })
    },

    fetch : async (country = '', update = true) => {

        return new Promise(async (resolve, reject) => {

            try {

                if (!country) return resolve({ status: false, data: 'Country Not Set' });

                let messages = [];
                
                let all = await exigo.query('exigo/messages.sql', country, 1);

                if(all.length < 1) return resolve({status : false, data : 'No pending messages'});

                all.forEach(row => messages.push({ id : row.id, phone: row.phone, text: row.text }));

                if(country.toUpperCase() == 'NG'){

                    for (let row of messages) 
                        await infobip.send(row);
                }

                if(country.toUpperCase() == 'GH'){

                    for (let row of messages) 
                        await mtn.send(row);
                }
                
                let ids = '';
                messages.forEach(row => (ids += `'${row.id}',`));
                let modified_date = moment().format('YYYY-MM-DD HH:mm:ss');

                if(update == true){

                    let query = `update customservicescontext.smsmessages set smsstatusid = 2, modifieddate = '${modified_date}' where messageid in (${ids.substr(0, ids.length - 1)})`;

                    await exigo.query(query);
                }

                if(['NG', 'GH'].includes(country)){

                    resolve({status: true, data: `${messages.length} Sent successfully`})

                }else{

                    resolve({ status: true, data: messages });
                }

            } catch (ex) {

                reject(ex);

            }
        })
    },

    report : async (data) => {

        return new Promise(async (resolve, reject) => {

            try{

                let country = data.country;
                let messages = data.messages;

                if(!country || !messages)
                    return resolve({status : false, data: 'Report Data sent is not correct' });

                if(messages.length < 1)
                    return resolve({status : false, data : 'There are no messages sent '});

                let messages_csv = [];

                messages.forEach(row => {

                    messages_csv.push({

                        'PHONE NUMBER' : row.address,
                        'DATE' : moment(row.date_sent).format('YYYY-MM-DD HH:mm:ss'),
                        'TIMESTAMP' : row.date_sent,
                        'READ' : row.read == 1 ? 'YES' : 'NO',
                        'MESSAGE' : row.body
                    });

                });

                jsonexport(messages_csv, (err, csv) => {

                    if (err) return res.json({ status: false, data: err });
                    
                    fs.writeFile(`./public/smsreport.csv`, csv, err => {

                        if (err) return resolve({ status: false, data: 'An Error Occured.' });

                        mailgun.sendWithAttachment({

                            to: config.get('sms.report_emails'),
                            subject: `Most Recent 100 SMS delivered by ${country}`,
                            body: `Kindly find attached, Most recent 100 delivered sms in ${country}`,
                            file: `smsreport.csv`

                        });

                        return resolve({status : true, data : 'Report sent successfully.'});

                    });

                });

            }catch(ex){

                reject(ex);
            }
        })
    },

    getCustomersFromNeolifeIds : async (neolife_ids = '')  => {

        return new Promise(async (resolve, reject) => {

            try{

                neolife_ids = neolife_ids.split(',').map(row => `'${row.trim()}'`).join(',');

                let customers = await exigo.query(`select customerid, mobilephone, field2 as name, field1 as neolife_id from customers where field1 in (${neolife_ids})`);

                resolve(customers);

            }catch(ex){

                reject(ex);
            }
        })
    },

    insertMessages: async (customers = [], message = '') => {

        return new Promise(async (resolve, reject) => {

            try{

                if(customers.length < 1)
                    return reject(`Customers must be at least 1 or more.`);

                if(message.length < 1)
                    return reject('Message is required.');

                let message_date = moment().format('YYYY-MM-DD HH:mm:ss');
                let query = 'insert into customservicescontext.smsmessages (messagedate, customerid, smsstatusid, fromnumber, tonumber, message, fieldname, keyvalue) values'

                customers.forEach(row => {

                    if(row.mobilephone.length > 1)
                        query+= `('${message_date}', ${row.customerid}, 0, 0, '${row.mobilephone}', '${message}', 'BulkSMS', 0),`;
                });

                query = query.substr(0, query.length - 1);

                let response = await exigo.query(query);

                resolve(customers.length);

            }catch(ex){

                reject(ex);
            }
        })
    }
}