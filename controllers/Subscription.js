const mailgun = require('../providers/mailgun');
const exigo = require('../providers/exigo');

const usernames = [
    
    'africa_it_olaiyas',
    'africa_it_ogochukwuo',
    'africa_it_johna',
    'africa_it_edwardm',
    'za_dr_kimh',
    'za_dr_michael',
    'ng_op_collinso'
];

const Subscription = {

    create: async (data = null) => {

        return new Promise(async (resolve, reject) => {

            try{

                let { neolife_id, subscription_id, days, description, user } = data;

                if(!neolife_id || !subscription_id || !days || !user || !description)
                    return reject({status: false, data: 'Invalid Request. All fields are required. '});

                if(!usernames.includes(user))
                    return reject({status: false, data: 'You are not Authorized to give subscription! You will be reported to Abiodun Sobowale.' });

                let customers = await exigo.query('exigo/customer.sql', neolife_id);

                if(customers.length < 1)
                    return reject({status: false, data: `Customer with Neolife ID ${neolife_Id} Does not Exist.`});

                let customer = customers[0];
                
                let create_subscriptions = await exigo.query(`select top 1 customerid from customservicescontext.createsubscriptions where customerid = ${customer.customerid} and subscriptionid = ${subscription_id}`);

                description =  `${description} - Created by ${user} ${new Date().toISOString()}`;

                if(create_subscriptions.length < 1){

                   let create_new_subscripton = await exigo.query('exigo/create_subscription.sql', customer.customerid, subscription_id, customer.neolife_id, days, description);
                }

                let manage_subscription = await exigo.query('exigo/manage_subscription.sql', customer.customerid, subscription_id, customer.neolife_id, days, description);

                mailgun.send({ to: ['oluwaseguno@gn.neolife.com'], subject: `New Subscription has been created from office.neolifeafrica.info`, body: `New Subscription of ${days} Days has been created for ${customer.neolife_id} on MDW. By ${user}` });

                return resolve({status: true, data: 'Subscription Created Successfully.'});

            }catch(ex){

                reject({status: false, data: ex });
            }
        })
    }

}

module.exports = Subscription;